import os
from glob import glob
import json

if __name__ == '__main__':
    repo_folder = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    config_folder = os.path.join(repo_folder, 'resources', 'configurations')
    
    print('List of updated paths:')
    config_paths = glob(os.path.join(config_folder, '*.json'))
    for path in config_paths:
        with open(path, 'r') as f:
            config = json.load(f)
        workers = config['workers']['collection_dict'].keys()
        if 'clf_pix' in workers:
            config['workers']['collection_dict']['clf_pix'] = config['workers']['collection_dict']['clf_pix'].replace('/path/to/magic_tools', repo_folder)
        if 'clf_mni' in workers:
            config['workers']['collection_dict']['clf_mni'] = config['workers']['collection_dict']['clf_mni'].replace('/path/to/magic_tools', repo_folder)

        print(config['workers']['collection_dict']['clf_pix'])
        print(config['workers']['collection_dict']['clf_mni'])
        with open(path, 'w') as f:
            json.dump(config, f)