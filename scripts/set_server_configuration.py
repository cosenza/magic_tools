import requests
import argparse

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    
    ap.add_argument('-c', '--config', required=True, help='path to config file')
    ap.add_argument('-s', '--server', required=False, help='server url address')
    ap.add_argument('-p', '--port', required=False, help='server port')

    args = vars(ap.parse_args())
    
    config_path = args['config']
    server_url = args['server']
    port = args['port'] if args['port'] is not None else '10123'

    url = server_url + ':' + port + '/set_config'

    try:
        check = requests.get(server_url + ':' + port + '/docs')
        if check.status_code == 200:
            print('server is online')
        else:
            raise RuntimeError('server does not respond correctly')
    except:
        raise RuntimeError('error during connection to server, check server url again: {}'.format(server_url + ':' + port))
    
    res = requests.post(url, json={'filepath': config_path})

    if res.status_code == 200:
        print('server configuration set successfully.')
    else:
        raise RuntimeError('server configuration failed. response status code: {}'.format(res.status_code))
            
    



    
    
