# magic_tools

Functions and classes to support MAGIC (**M**achine-learning **A**ssisted **G**enomic and **I**maging **C**onvergence)

Main features:
- view images, segmentations and labels
- tools for semantic and instance annotation 
- training and testing of XGBoost machine-learning models
- image processing server for automated microscopy


## Installation

Clone the repository  
`git clone https://git.embl.de/cosenza/magic_tools/ && cd magic_tools`  

Create a virtual environment (e.g. via conda or venv), magic_tools was tested with Python 3.11.5 on Ubuntu 22.04.4 LTS.  
`conda create -n magic python=3.11.5 jupyterlab ipywidgets && conda activate magic`

Activate the environment and install the pip requirements file  
`python3 -m pip install -r requirements.txt`  

Then install magic_tools  
`pip install -e .`  

If you want to use it in jupyter notebooks, do not forget to install the kernelspec  
`python3 -m ipykernel install --user --name magic`  

Also, to run the image processing server you need to install uvicorn:  
`sudo apt install uvicorn`


## Getting started

To get familiar with magic_tools, you can find a series of jupyter notebooks, with tutorials taking you through the main features of the package.  

### notebooks
Tutorials can be found in the **notebooks** folder:

- [**semantic_segmentation_example.ipynb**](https://git.embl.de/cosenza/magic_tools/-/blob/main/notebooks/semantic_segmentation_example.ipynb)  
this tutorial shows how to extract pixel-level features and create a pixel classifier for semantic segmentation

- [**object_classification_example.ipynb**](https://git.embl.de/cosenza/magic_tools/-/blob/main/notebooks/object_classification_example.ipynb)  
this tutorial shows how to extract object-level features and create a classifier for object classification

- [**inference_server_example.ipynb**](https://git.embl.de/cosenza/magic_tools/-/blob/main/notebooks/inference_server_example.ipynb)  
this tutorial shows how to interact with the image processing server, sending images and receiving inferences

- [**annotation_tools.ipynb**](https://git.embl.de/cosenza/magic_tools/-/blob/main/notebooks/annotation_tools.ipynb)  
this tutorial teaches you how to use tools for pixel and object-level annotation

### scripts
The **script** folder contains some useful tools:

- [**update_configuration_paths.py**](https://git.embl.de/cosenza/magic_tools/-/blob/main/scripts/update_configuration_paths.py)  
this script programmatically updates absolute paths in configuration files to adjust to your current repository folder  
`python update_configuration_paths.py`  

- [**set_server_configuration.py**](https://git.embl.de/cosenza/magic_tools/-/blob/main/scripts/set_server_configuration.py)  
this script allows you to quickly set a configuration for the image processing server  
`python set_server_configuration.py -c /path/to/config/file -s server_url -p port`  

- [**test_server_configurations.py**](https://git.embl.de/cosenza/magic_tools/-/blob/main/scripts/test_server_configurations.py)  
this script tests available configurations to make sure they are working as expected    
`python test_server_configurations.py -c /path/to/config/folder -s server_url -p port -i /path/to/test/image`  

- [**collect_micronuclei_features.py**](https://git.embl.de/cosenza/magic_tools/-/blob/main/scripts/collect_micronuclei_features.py)  
this script runs a batch image processing using the server for image processing, to generate label images and feature tables of micronuclei objects. Useful in the process of generating a new training set.  


## Launching the server

### launch the server
The server can be launch with uvicorn.  
First open a terminal and navigate to the repository folder  
`cd /path/to/repository/magic_tools/`

Then start the server  
`uvicorn magic_tools.server.main:app` 

You can also define host and port, for example if you run it on a cluster node and want to expose it to the intranet:  
`uvicorn magic_tools.server.main:app --host 0.0.0.0 --port 10123`

You will see a confirmation that the server is running at the specified url
```
INFO:     Started server process [163403]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
INFO:     Uvicorn running on http://0.0.0.0:10123 (Press CTRL+C to quit)
```

### configure the server
To learn how to interact with the server and how to configure it for an image analysis task, check out the notebook tutorial **inference_server_example.ipynb** in the notebooks folder.  
Note that configuration files can be found in the `resources/configurations` folder, but **classifier paths need to be adjusted** to absolute paths that are valid for the machine where the server is running (see [update_configuration_paths.py](https://git.embl.de/cosenza/magic_tools/-/blob/main/scripts/update_configuration_paths.py) for more information).

### dashboard
The server also has a dashboard, where you can monitor statistics about incoming jobs, execution time and prediction outputs.  
It can be accessed via the **dashboard** endpoint: http://0.0.0.0:10123/dashboard 


## Contributors

- Marco Raffaele Cosenza (maintainer and developer)

## Contact

Please, contact Marco Raffaele Cosenza (marco.cosenza(at)embl.de) in case you have any questions.