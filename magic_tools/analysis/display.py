import warnings
import numpy as np
import scipy.ndimage as ndi
import matplotlib.pyplot as plt
import cv2
from skimage.morphology import disk
from tqdm import tqdm
from .image import CropRoi, ImageSet
import napari

    
def matshow(loader, df, crop_size=None, 
            track=None, 
            filename_col = 'filename',
            id_col = 'obj_id',
            n_cols=None, n_rows=None, 
            edge=None):

    track = loader.track if track is None else track
    crop_size = loader.crop_size if crop_size is None else crop_size

    # retrieve rois
    roi_list = []
    groups = df.groupby(filename_col)
    for filename, group in tqdm(groups):
        img = loader.get_image(filename)
        img.crop_size = crop_size
        rois = img.get_roi(group[id_col].values, track=track, square=True)
        rois = [rois] if isinstance(rois, CropRoi) else rois
        roi_list.extend(rois)
    
    # DEFINE N_COLS AND N_ROWS
    if (n_cols is None) and (n_rows is None):
        n_cols = 10 if len(roi_list) >= 10 else len(roi_list)
        n_rows = np.ceil(len(roi_list)/n_cols).astype(int)
    elif n_cols is None:
        n_cols = np.ceil(len(roi_list)/n_rows).astype(int)
    elif n_rows is None:
        n_rows = np.ceil(len(roi_list)/n_cols).astype(int)
    if n_rows*n_cols <= len(roi_list):
        warnings.warn('Showing less images than available in the stack')

    # generate image
    if edge is None:
        for layer in ['channel', 'mask']:
            mat = roimat(roi_list, layer=layer, crop_size=crop_size)
            fig, ax = plt.subplots(figsize=(3*n_cols, 3*n_rows))
            ax.matshow(mat)
            ax.set_axis_off()
    elif edge:
        layer_mat = roimat(roi_list, layer='channel', crop_size=crop_size)
        edge_mat = roimat(roi_list, layer=edge, crop_size=crop_size) > 0
        edge_mat = cv2.bitwise_xor(cv2.dilate(edge_mat.astype(np.uint8), kernel=disk(1), iterations=1), edge_mat.astype(np.uint8))
        
        fig, ax = plt.subplots(figsize=(3*n_cols, 3*n_rows))
        ax.matshow(layer_mat)
        ax.matshow(np.ma.array(edge_mat, mask= edge_mat==0), cmap='prism')
        ax.set_axis_off()
        ax.set_axis_off()
    
    return fig, ax
    
def roimat(roi_list, layer='channel', n_cols=None, n_rows=None, crop_size=None):
    # DEFINE N_COLS AND N_ROWS
    if (n_cols is None) and (n_rows is None):
        n_cols = 10 if len(roi_list) >= 10 else len(roi_list)
        n_rows = np.ceil(len(roi_list)/n_cols).astype(int)
    elif n_cols is None:
        n_cols = np.ceil(len(roi_list)/n_rows).astype(int)
    elif n_rows is None:
        n_rows = np.ceil(len(roi_list)/n_cols).astype(int)
    if n_rows*n_cols <= len(roi_list):
        warnings.warn('Showing less images than available in the stack')
    
    if crop_size is None:
        crop_size = roi_list[0].channel.shape[0]

    # STITCH IMAGES INTO MATRIX
    n = 0
    mat = []
    for nr in range(n_rows):
        row = []
        for nc in range(n_cols):
            # stitch black image
            if n >= len(roi_list):
                row.append(np.zeros((crop_size, crop_size)))
            else:
                row.append(roi_list[n].__getattribute__(layer))
            n += 1
            
        row = np.concatenate(row, axis=1)
        mat.append(row)

    mat = np.concatenate(mat, axis=0) if len(mat) > 1 else row

    return mat
    
def napari_view(imgset: ImageSet):
    v = napari.Viewer()
    for ltype, ldict in imgset.layers.items():
        for lname, lcontent in ldict.items():
            v.add_image(lcontent, name='_'.join([ltype,lname]))