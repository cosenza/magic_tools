import os
import glob
from typing import Any
import warnings
import cv2
import numpy as np
import pandas as pd
import scipy.ndimage as ndi
from skimage.io import imread
from skimage.segmentation import clear_border
from skimage.morphology import disk

from .utils import clip_coordinates, slice_neighborhood


class CropRoi():
    '''Region-of-interest (Roi) class.
    Each roi has 1 channel, 1 segmentation, 1 label layer.
    Additionally it stores its original name, label_id, center coordinates and size of the neighborhood.'''
    
    def __init__(self, channel, name=None, label=None, segmentation=None, mask=None, label_id=None, coordinates=None, neighborhood=None):
        self.channel = channel
        self.name = name
        self.segmentation = segmentation
        
        self.label = label
        self.label_id = label_id
        self.mask = mask if mask is not None else self.label == self.label_id
        
        self.coordinates = coordinates
        self.neighborhood = neighborhood

    def pixels(self):
        '''Returns the array of pixel intensities contained in the mask'''
        return self.channel[self.mask]

    def zero_out(self):
        '''Returns the channel where all pixels outside of the mask are set to zero.'''
        img = self.channel.copy()
        img[~self.mask] = 0
        return img
    
    def get_edge(self, size=1):
        '''Returns the segmentation of the mask edge.'''
        edge = ndi.binary_dilation(self.mask, structure=disk(size)) ^ self.mask
        return edge

    def copy(self):
        '''Returns a copy of the roi'''
        roi_params = {
            'name': self.name,
            'channel': None,
            'mask': None,
            'segmentation': None,
            'label': None,
            'label_id': self.label_id,
            'coordinates': self.coordinates,
            'neighborhood': self.neighborhood,
        }
        for k in ['channel', 'mask', 'segmentation', 'label']:
            if self.__getattribute__(k) is not None:
                roi_params[k] = self.__getattribute__(k).copy()
        return CropRoi(**roi_params)    
        # return CropRoi(name=self.name, channel=self.channel, mask=self.mask, segmentation=self.segmentation, label=self.label,
        #                label_id=self.label_id, coordinates=self.coordinates, neighborhood=self.neighborhood)



LAYER_TYPES = pd.DataFrame.from_dict({
    'types': ['channels', 'segmentations', 'labels', 'predictions', 'annotations'],
    'single': ['c', 's', 'l', 'p','a'],
    'dtype': [[np.uint8, np.uint16], [bool], [np.uint16], [np.float64], [np.uint16]]})

class ImageSet(object):
    def __init__(self, name=None, layers=None, tracks=None, channels=None, segmentations=None, labels=None, predictions=None, annotations=None):
        '''ImageSet is a collection of images and image-related arrays organized in layers and tracks.
        Layers organize data into channels, segmentations, labels, predictions and annotations, while tracks tie together data between layers.

        Parameters
        ----------
        name: str
            name of the ImageSet (e.g. filename)
        layers: dict
            dictionary organizing the layers. Its keys can be only layer types: ['channels', 'segmentations', 'labels', 'predictions', 'annotations']
            for more information on how to define layers, see below.
        tracks: pandas.Dataframe
            dataframe pre-defining tracks. Dataframe columns are ['track', 'channel', 'segmentation', 'label', 'prediction', 'annotation']
            for more information on how to define tracks, see below.
        channels, segmentations, labels, predictions, annotations: dict of numpy.ndarray
            dictionary having identifier names as keys and numpy arrays as values for a given layer type

        There are 5 layer types available: ['channels', 'segmentations', 'labels', 'predictions', 'annotations'].
        Each layer type can contain multiple layers, where the content of the layer is a numpy array and is identified by a unique name. 
        A single layer type is defined as a dictionary: {'name1': array1, 'name2': array2 ... 'namen': arrayn}
        e.g. for the channels 
        channels = {'ch1': arr1, 'ch2': arr2}
        To define all layers together:
        layers = {'channels': {'ch1': arr1, 'ch2': arr2}, 'segmentations': {'seg1': seg1, 'seg2': seg2} ...}
        the order of layers within a layer type is not important, but layer names have to be unique

        Tracks can string together one layer from each layer type, identified by their layer names
        Tracks are identified by their unique name and are a collection of layer names, one per each layer type.
        Therefore, only one layer name is allowed for each layer type.
        Tracks are desribed in a dataframe table with layer types as columns ['track', 'channel', 'segmentation', 'label', 'prediction', 'annotation']
        and track definitions as rows.
        '''    

        if not np.logical_xor((layers is not None), any([i is not None for i in [channels, segmentations, labels, predictions, annotations]])):
            raise ValueError('Wrong initialization: use layers argument with nested dictionary or layer type arguments individually')
        
        if layers is None:
            layers = {}
            alldicts = [i for i in [channels, segmentations, labels, predictions, annotations] if i is not None]
            for ltype, ldict in alldicts.items():
                layers[ltype] = ldict

        # the dynamic attributes are initialized with the object method
        # to avoid infinite recursion
        object.__setattr__(self, 'layers_names', [])
        object.__setattr__(self, 'channels', {})
        object.__setattr__(self, 'segmentations', {})
        object.__setattr__(self, 'labels', {})
        object.__setattr__(self, 'predictions', {})
        object.__setattr__(self, 'annotations', {})
        object.__setattr__(self, 'layers', layers)

        self.name = name
        self.shape = None # just the 2D shape
        self.height = None
        self.width = None
        self.dimensions = None
        self.crop_size = None
        self.tracks = pd.DataFrame(columns=['track', 'channel', 'segmentation', 'label', 'prediction', 'annotation']) if tracks is None else tracks
        self.track = None

        self.layers = layers
        
        self.parse_layers()
        if 'labels' in self.layers.keys():
            self.parse_labels()
        self.set_image_size()

    # GET ATTRIBUTE FUNCTIONS
    # all data is stored in self.layers
    # layer types and single layers are obtained dynamically from self.layers
    # saving memory, whilst also being listed as attributes 
    # (and autocomplete after dot)

    def __getattribute__(self, __name: str):
        if __name in ['channels', 'segmentations', 'labels', 'predictions', 'annotations']:
            return self.get_layer_type(__name)
        elif __name in object.__getattribute__(self, 'layers_names'):
            return self.get_layer(__name)
        else:
            return object.__getattribute__(self, __name)

    def get_layer_type(self, ltype: str):
        if ltype in self.layers:
            return object.__getattribute__(self, 'layers')[ltype]
        else:
            return {}

    def get_layer(self, lname: str):
        for d in object.__getattribute__(self, 'layers').values():
            if lname in d.keys():
                return d[lname]
            
    # SET ATTRIBUTE FUNCTIONS
    # when one of the attributes referring to self.layers is changed
    # this change is caught up, self.layers is changed 
    # and all attributes are remapped onto the new layers keys
    def __setattr__(self, __name: str, __value: Any):
        # change self.layers
        # update labels if needed
        if __name == 'layers':
            object.__setattr__(self, 'layers', __value)
            self.parse_layers()
            self.parse_labels()
            self.set_image_size()
        elif __name in ['channels', 'segmentations', 'labels', 'predictions', 'annotations']:
            self.layers[__name] = __value
            if __name == 'labels':
                self.parse_labels()
            self.parse_layers()
            self.set_image_size()
        elif __name in self.layers_names:
            for ltype, ldict in self.layers.items():
                for lname in ldict.keys():
                    if __name == lname:
                        self.layers[ltype][__name] = __value
                    if ltype == 'labels':
                        self.parse_labels()
                        break
                break

            # because layers are got dynamically, 
            # we need to catch the change and update self.layers
            # reset layer attributes
            
            self.parse_layers()
            self.parse_labels()
            self.set_image_size()
        else:
            object.__setattr__(self, __name, __value)

    # RESET FUNCTIONS
    # Reorganize all attributes when a change occurs
    def parse_layers(self):
        for ln in self.layers_names:
            delattr(self, ln)
        self.layers_names = []

        updated_layers = {}
        for ltype, ldict in self.layers.items():
            # check validity of layer type keys
            if not np.logical_or(ltype in LAYER_TYPES['types'].values, ltype in LAYER_TYPES['single'].values):
                raise ValueError('Invalid layer type key: {}'.format(ltype))
            # convert layer type abbreviations to full type names
            if ltype in LAYER_TYPES['single'].values: 
                ltype_full = LAYER_TYPES.loc[LAYER_TYPES['single'] == ltype, 'types'].iloc[0]
                updated_layers[ltype_full] = ldict
            else:
                updated_layers[ltype] = ldict

            # add attributes and layer names
            for lname in ldict.keys():
                if lname in self.layers_names:
                    raise ValueError('layer names must be unique: {}'.format(lname))
                self.__setattr__(lname, None)
                self.layers_names.append(lname)
        object.__setattr__(self, 'layers', updated_layers)

    def parse_labels(self):
        self.label_idxs = {l: np.unique(i) for l,i in self.labels.items()}
        self.n_labels = {k: len(v) for (k,v) in self.label_idxs.items()} # in case labels are sparse
        self.labels_coordinates = {}
        for ch in self.labels:
            self.labels_coordinates[ch] = self.get_label_coordinates(ch)
        self.labels_bboxes = {}
        for ch in self.labels:
            self.labels_bboxes[ch] = self.get_label_bboxes(ch)
    
    def set_image_size(self):
        '''Define image size (width and height) and checks all layers dimensions match.'''
        n = 0
        for ldict in self.layers.values():
            for lcontent in ldict.values():
                if self.shape is None:
                    self.dimensions = lcontent.shape
                    self.shape = lcontent.shape[-2:]
                    self.height, self.width = self.shape
                elif self.dimensions != lcontent.shape:
                    raise ValueError('layers have unequal dimensions {} instead of {}'.format(self.dimensions, lcontent.shape))
    
    # LABEL LAYER FUNCTIONS
    def get_label_coordinates(self, label):
        if self.n_labels[label] > 1:
            coords = ndi.center_of_mass(self.labels[label] != 0, labels=self.labels[label], index=self.label_idxs[label])
            return {k:v for k,v in zip(self.label_idxs[label], coords)}
        else:
            return {}

    def get_label_bboxes(self, label):
        if self.n_labels[label] > 1:
            bboxes = ndi.find_objects(self.labels[label])
            bboxes = {n:b for (n,b) in zip(np.arange(1,len(bboxes)+1), bboxes) if b is not None}
            return bboxes
        else:
            return {}

    def labels_from_segmentation(self, segmentation, labels_name, exclude_border=True):
        '''Convert segmentation layer to label layer by connected component analysis.
        
        Parameters
        ----------
        segmentation: str
            name of the segmentation layer to convert
        labels_name: str
            name of the label layer to be created
        exclude_border: bool
            exclude objects touching borders from the conversion'''
        
        if segmentation not in self.layers_names:
            raise RuntimeError('Invalid layer name for segmentation: {}'.format(segmentation))
        segm = self.layers['segmentations'][segmentation]
        segm = clear_border(segm) if exclude_border else segm
        if 'labels' not in self.layers:
            self.layers['labels'] = {} # not to update layers 2 times
        _, self.layers['labels'][labels_name] = cv2.connectedComponents(segm.astype(np.uint8))
        self.parse_layers()
        self.parse_labels()

    # TRACKS FUNCTIONS
    def add_track(self, name, channel=None, segmentation=None, label=None, annotation=None, prediction=None):
        if self.tracks['track'].str.contains(name).any():
            raise ValueError('Track name already exists: {}'.format(name))

        track = pd.DataFrame.from_dict({
            'track': [name], 'channel': [channel], 'segmentation': [segmentation], 
            'label': [label], 'annotation': [annotation], 'prediction': [prediction]})
        self.tracks = pd.concat([self.tracks, track], axis=0)
        self.tracks.index = np.arange(len(self.tracks))

    # GET ITEM FUNCTIONS
    def get_stack(self):
        '''returns all available layers as numpy array of dimensions (total_layer_number, height, width)'''
        return np.stack([v for l in self.layers.values() for v in l.values()], axis=0)

    def get_crop(self, x, y, clip=False):
        '''returns a crop centered on coordinates (x,y) for each available layer.
        the output is a numpy array of dimensions (total_layer_number, crop_size, crop_size).
        
        Parameters
        ----------
        x: int
            center x coordinate
        y: int
            center y coordinate
        clip: bool
            clip the crop square to image borders if crop exceeds image dimensions
        
        Returns
        -------
        crop: numpy.ndarray
            array of dimensions (total_layer_number, crop_size, crop_size)
            '''
        size = int(self.crop_size/2)

        if clip:
            x,y = clip_coordinates(x,y, self.shape, self.crop_size//2)
            view = self.get_stack()[:, (y-size):(y+size), (x-size):(x+size)]
        else:
            ystart = y-size if y-size >= 0 else 0
            ystop = y+size if y+size < self.height else self.height
            xstart = x-size if x-size >= 0 else 0
            xstop = x+size if x+size < self.width else self.width
            view = self.get_stack()[:, ystart:ystop, xstart:xstop]
        
        # must return a copy of the array not a view
        crop = view.copy()
        return crop

    def get_label(self, label_id, label=None, clip=False):
        '''returns a crop centered on the centroid of object with label_id for each available layer.
        the output is a numpy array of dimensions (total_layer_number, crop_size, crop_size).
        The function is analogous to get_crop, where coordinates are defined by object centroids.
        
        Parameters
        ----------
        label_id: int
            identifier for the object to retrieve
        label: str
            label layer name. If not defined, it defaults to label name of the current active track
        clip: bool
            clip the crop square to image borders if crop exceeds image dimensions
        
        Returns
        -------
        crop: numpy.ndarray
            array of dimensions (total_layer_number, crop_size, crop_size)
            '''
        if label is None:
           label = self.tracks[self.tracks['track'] == self.track]['label'].values[0]
        y,x = self.labels_coordinates[label][label_id]
        crop = self.get_crop(x=int(x),y=int(y),clip=clip)
        i = self.layers_names.index(label)
        mask = crop[i,:,:] == int(label_id)
        crop[i,:,:] = mask
        return crop

    def __getitem__(self, index):
        
        # INDEXING BY COORDINATES
        # selection by coordinates is done by tuple
        # indexing by coordinate
        if isinstance(index, tuple) and len(index)==2 and all([isinstance(i, (int, slice)) for i in index]):
            if all([isinstance(i, int) for i in index]):
                return self.get_crop(index[0], index[1], clip=False)
            else:
                return self.get_stack()[:, index[0], index[1]].copy()
        # list of coordinates
        elif isinstance(index, tuple) and all([len(i) == 2 for i in index]) and all([isinstance(i, tuple) for i in index]):
            return np.array([self.get_crop(coord[0], coord[1], clip=True) for coord in index]) 

        # INDEXING BY LABEL
        if self.track is not None:
            label = self.tracks[self.tracks['track'] == self.track]['label'].values[0]
        elif self.labels is not None:
            label = next(iter(self.labels)) # first key of dict
            warnings.warn('track not selected, indexing on label {}'.format(label))

        idxs = list(self.labels_coordinates[label].keys())
        # a single integer is interpreted as a single label
        if isinstance(index, int):
            return self.get_label(label_id=index, label=label, clip=True)
        # selection by list of integers or boolean array or slice
        elif (isinstance(index,list) and all([isinstance(i, (int, bool)) for i in index])) or isinstance(index,slice):
            selected = list(np.array(idxs)[index])
            return np.array([self.get_label(label_id=i, label=label, clip=True) for i in selected])
        else:
            raise ValueError('invalid index')
    
    def get_roi(
        self, label_id=None, track=None, channel=None, label=None, segmentation=None, prediction=None, 
        annotation=None, neighborhood=20, square=False):
        '''Returns a CropRoi for the object with the corresponding label_id and track.
        If track is not defined, the specific layers have to be specified for each relevant layer type.
        
        Parameters
        ----------
        label_id: list of int
            list containing all identifiers for the objects to retrieve
        track: str
            track name, which will define layers to retrieve
        channel, label, segmentation, prediction, annotation: str
            layer name to include in the CropRoi for each layer type
        neighborhood: int
            number of pixels to include, surrounding the bounding box
        square: bool
            make a square crop. If True it uses the crop_size as side length for the square

        Returns
        -------
        rois: list of magic_tools.analysis.image.CropRoi
            list of CropRoi objects
        '''

        roi_params = {
                'channel' : channel,
                'segmentation' : segmentation,
                'label' : label,
                'prediction' : prediction,
                'annotation' : annotation
            }
        # parse arguments
        if track is not None:
    
            track = self.tracks[self.tracks['track'] == track].iloc[0,:].to_dict()
            for k in roi_params.keys():
                roi_params[k] = track[k]
        elif label is None:
            raise ValueError('track or label has to be specified')
        label = roi_params['label']

        # switch layer names for layer content
        for k,v in roi_params.items():
            if v is None:
                continue
            else:
                roi_params[k] = self.__getattribute__(k+'s')[v]
        
        # remove 0 from labels, as find_objects ignores label 0
        if label_id is None:
            label_id = self.label_idxs[label]
        label_id = [l for l in label_id if l != 0]

        if len(label_id) == 0:
            return []

        if square:
            
            rois = []

            for idx in label_id:
                y,x = self.labels_coordinates[label][idx]
                size = self.crop_size//2
                x,y = clip_coordinates(x,y, self.shape, size)
                bbox = (slice((y-size),(y+size), None), slice((x-size),(x+size), None))
                
                kwargs = {}
                for k,v in roi_params.items():
                    if v is None:
                        continue
                    else:
                        kwargs[k] = v[bbox].copy()

                roi = CropRoi(
                    name = self.name,
                    **kwargs,
                    label_id = idx,
                    coordinates = self.labels_coordinates[label][idx],
                    neighborhood=neighborhood)
                rois.append(roi)

        else:
            # get bounding boxes
            bboxes = [self.labels_bboxes[label][i] for i in label_id]
            
            if neighborhood > 0:
                bboxes = [slice_neighborhood(bbox, size=neighborhood, img_shape=self.shape) for bbox in bboxes]
            
            rois = []
            for idx, bbox in zip(label_id, bboxes):
                kwargs = {}
                for k,v in roi_params.items():
                    if v is None:
                        continue
                    else:
                        kwargs[k] = v[bbox].copy()
                        
                roi = CropRoi(
                    name = self.name,
                    **kwargs,
                    label_id = idx,
                    coordinates = self.labels_coordinates[label][idx],
                    neighborhood=neighborhood)
                rois.append(roi)
        return rois if len(rois) > 1 else rois[0]


class ImageLoader():
    def __init__(self, dict_paths, dict_prefixes=None, tracks=None):

        self.dict_prefixes = {} if dict_prefixes is None else dict_prefixes
        self.dict_paths = dict_paths
        self.filepaths = {}
        self.layers_names = []

        for ltype, ldict in self.dict_paths.items():
            layer_type = ltype
            if not np.logical_or(ltype in LAYER_TYPES['types'].values, ltype in LAYER_TYPES['single'].values):
                raise ValueError('Invalid layer type key: {}'.format(ltype))
            # convert layer type abbreviations to full type names
            if ltype in LAYER_TYPES['single'].values: 
                layer_type = LAYER_TYPES.loc[LAYER_TYPES['single'] == ltype, 'types'].iloc[0]
            path_dict = {}
            for lname, path in ldict.items():
                path_dict[lname] = glob.glob(path)
                self.layers_names = lname
            self.filepaths[layer_type] = path_dict
        
        self.filepath_table = None
        self.combine_filepaths()

        self.channels = None
        self.segmentations = None
        self.labels = None
        self.tracks = pd.DataFrame(columns=['track', 'channel', 'segmentation', 'label', 'annotation', 'prediction']) if tracks is None else tracks
        self.track = None

        self.n = 0

    def add_track(self, name, channel=None, segmentation=None, label=None, annotation=None, prediction=None):
        assert not self.tracks['track'].str.contains(name).any()
        track = pd.DataFrame.from_dict({'track': [name], 'channel': [channel], 'segmentation': [segmentation], 'label': [label],
                                        'annotation': [annotation], 'prediction': [prediction]})
        self.tracks = pd.concat([self.tracks, track], axis=0)
        self.tracks.index = np.arange(len(self.tracks))

    def combine_filepaths(self):
        # preprocess basenames
        filepath_dfs = []
        for ltype, ldict in self.filepaths.items():
            for lname, paths in ldict.items():
                basenames = [os.path.basename(p) for p in paths]
                if lname in self.dict_prefixes.keys():
                    basenames = [b.replace(self.dict_prefixes[lname], '') for b in basenames]
                df = pd.DataFrame.from_dict({'basename':basenames, '_'.join([ltype,lname]): paths}) # layer type and name combined in column title
                filepath_dfs.append(df)

        # merge by basenames
        self.filepath_table = filepath_dfs[0]
        for df in filepath_dfs[1:]:
            self.filepath_table = pd.merge(self.filepath_table, df, on='basename', how='outer')
        self.filepath_table.sort_values(by='basename', inplace=True)
        self.filepath_table.reset_index(drop=True, inplace=True)
    
    def get_image(self, basename):
        sel = (self.filepath_table['basename'] == basename)
        if sel.any() == False:
            raise ValueError(f'{basename} does not exist in loader file list')
        idxs = self.filepath_table.loc[sel,:].index
        return self.__getitem__(idxs)

    def __getitem__(self, index):
        index = [index] if isinstance(index, int) else index

        subdf = self.filepath_table.loc[index,:]
        cols = subdf.columns.difference(['basename'])

        items = []
        for n, row in subdf.iterrows():
            
            #FIXME instead of fillna with 0, just select column without na values
            row = subdf.iloc[0,:]      
            row.fillna(0, inplace=True)
            paths = row[cols]

            layers = {}
            for layer, path in paths.items():
                ltype, lname = layer.split('_')
                if path != 0:
                    if ltype not in layers.keys():
                        layers[ltype] = {}
                    layers[ltype][lname] = imread(path)
            img = ImageSet(name=row['basename'], layers=layers, tracks=self.tracks)
            items.append(img)
        return items[0] if len(items)==1 else items

    def __len__(self):
        return len(self.filepath_table)

    def __iter__(self):
        self.n = 0
        return self
 
    def __next__(self):
        if self.n >= self.__len__():
            raise StopIteration
        else:
            self.n += 1
            return self.__getitem__(self.n - 1)
