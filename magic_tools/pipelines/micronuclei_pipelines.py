import logging
import os
import time

import numpy as np
import pandas as pd

import cv2
from skimage.segmentation import clear_border

from .pipeline import Pipeline
from ..analysis import image  
from skimage.io import imsave  
from .. import __path__



class MicronucleiFeatures(Pipeline):
    def __init__(self, config):
        super().__init__(config)
        default_options = { 
            'pixel_segmentation_threshold': .5,
            'crop_size': 300,
            'isolated_threshold': 1500
            }
        for k,v in default_options.items():
            if k not in self.options.keys():
                self.options[k] = v

    def run(self, img, filename=None):

        if len(self.output) > 0:
            self.output = {}

        filename = 'img' if filename is None else filename
        
        # get workers and input
        pixext, objext, resizer, findnn, pix_clf = self.workers.get_workers()
        img = image.ImageSet(name=filename, layers={'c': {'mip': img}})

        times = {}
        t0 = time.time()

        # semantic pixel classification
        # feature extraction
        pix_fts = pixext.run(img, 'mip', dataframe=True)
        times['pixfts'] = time.time() - t0
        # prediction
        pred = pix_clf.predict(pix_fts)[:,1].reshape(img.shape)
        times['pixpred'] = time.time() - t0
        segm_raw = pred >= self.options['pixel_segmentation_threshold']
        # add segmentation
        img.segmentations = {'segmentation_raw': segm_raw}
        img.labels_from_segmentation(segmentation='segmentation_raw', labels_name='labels_raw', exclude_border=True)
        img.add_track('raw', channel='mip', segmentation='segmentation_raw', label='labels_raw')
        self.add_output('labels_raw', img.labels['labels_raw'])

        # resize rois
        img.crop_size = self.options['crop_size']
        times['roiprep'] = time.time() - t0
        lbl_nucl_smooth = resizer.run(img, 'raw')
        times['resizerois'] = time.time() - t0
        
        # add segmentations to image
        #img.labels = {**img.labels, **{'labels_nucl': lbl_nucl}}
        img.add_track('nucl', channel='mip', label='labels_raw') # we consider labels on raw segmentation as nuclei, not smoothened out versions
        
        # calculate nuclei features
        nucl_fts = objext.run(img, track='nucl')
        times['nuclfts'] = time.time() - t0

        # separate nuclei and micronuclei segmentations
        if ('area' in nucl_fts.columns) and (nucl_fts.shape[0] > 0):
            sel = (nucl_fts['area'] <= self.options['isolated_threshold'])
            isolated_idxs = nucl_fts.loc[sel, 'obj_id']
            segm_mni = (img.labels['labels_raw'] ^ lbl_nucl_smooth) > 0 # extracted micronuclei
            segm_isolated = np.isin(img.labels['labels_raw'], isolated_idxs.to_list()) # isolated micronuclei
            segm_mni = np.logical_or(segm_mni, segm_isolated) # combine extracted and isolated micronuclei
            lbl_nucl = ~segm_isolated * img.labels['labels_raw'] # remove isolated micronuclei from nuclei labels
        else:
            segm_mni = clear_border(segm_raw ^ (lbl_nucl_smooth>0))
            lbl_nucl = img.labels['labels_raw']

        # filter out small micronuclei areas
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
        segm_mni = cv2.erode(segm_mni.astype(np.uint8), kernel, iterations=1)
        segm_mni = cv2.dilate(segm_mni, kernel, iterations=1) > 0

        # update imageset
        img.layers = { 
            'channels': {
                **img.channels
                },
            'segmentations': {
                **img.segmentations, 
                'segmentation_mni': segm_mni
                },
            'labels': {
                'labels_nucl': lbl_nucl
                }
            }
        img.labels_from_segmentation(segmentation='segmentation_mni', labels_name='labels_mni', exclude_border=True)
        img.add_track('mni', channel='mip', segmentation='segmentation_mni', label='labels_mni')
        times['mnifilter'] = time.time() - t0

        # calculate micronuclei features
        mni_fts = objext.run(img, track='mni')
        times['mnifts'] = time.time() - t0

        self.add_output('labels_nucl', img.labels['labels_nucl'])
        self.add_output('labels_mni', img.labels['labels_mni'])

        # assign micronuclei-nuclei pairs
        times['labelsprep'] = time.time() - t0
        nn = findnn.run(img)
        self.add_output('nn', nn)
        times['findnn'] = time.time() - t0
        
        # merge features
        if (mni_fts.shape[0] == 0) or (nucl_fts.shape[0] == 0) or (not nn.columns.str.contains('seed_id').any()):
            merge = pd.DataFrame()
        else:
            merge = pd.merge(mni_fts, nn[['seed_id', 'neighbor_id', 'distance']], left_on='obj_id', right_on='seed_id', how='left')
            merge = pd.merge(merge, nucl_fts, left_on='neighbor_id', right_on='obj_id', how='left', suffixes=['_mni', '_nucl'])

            times['merges'] = time.time() - t0
        tf = time.time()

        self.add_output('nucl_fts', nucl_fts)
        self.add_output('mni_fts', merge)
        self.add_output('img', img)
        print('TOT TIME {}'.format(tf-t0))

        for k,v in times.items():
            print(k,v)

        # logging
        logging.debug('execution time: {:.2f}'.format(tf-t0))
        
        return True


class MicronucleiFeaturesWatershed(Pipeline):
    def __init__(self, config):
        super().__init__(config)
        default_options = { 
            'pixel_segmentation_threshold': .5,
            'crop_size': 300,
            'isolated_threshold': 1500
            }
        for k,v in default_options.items():
            if k not in self.options.keys():
                self.options[k] = v

    def run(self, img, filename=None):

        if len(self.output) > 0:
            self.output = {}

        filename = 'img' if filename is None else filename
        
        # get workers and input
        pixext, objext, resizer, findnn, wat, pix_clf, mni_clf = self.workers.get_workers()
        img = image.ImageSet(name=filename, layers={'c': {'mip': img}})

        times = {}
        t0 = time.time()

        # semantic pixel classification
        # feature extraction
        pix_fts = pixext.run(img, 'mip', dataframe=True)
        times['pixfts'] = time.time() - t0
        # prediction
        pred = pix_clf.predict(pix_fts)[:,1].reshape(img.shape)
        times['pixpred'] = time.time() - t0
        segm_raw = pred >= self.options['pixel_segmentation_threshold']

        # refine segmentation (close operation)
        segm_raw = cv2.dilate(segm_raw.astype(np.uint8), kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)), iterations=1)
        segm_raw = cv2.erode(segm_raw.astype(np.uint8), kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)), iterations=1)

        # add segmentation
        img.segmentations = {'segmentation_raw': segm_raw>0}
        img.labels_from_segmentation(segmentation='segmentation_raw', labels_name='labels_raw', exclude_border=True)
        img.add_track('raw', channel='mip', segmentation='segmentation_raw', label='labels_raw')
        self.add_output('labels_raw', img.labels['labels_raw'])

        # watershed
        wat_lbls = wat.run(img, track='raw')
        img.labels = {**img.labels, 'labels_nucl': wat_lbls}
        self.add_output('watershed', img.labels['labels_nucl'])
        img.add_track('nucl', channel='mip', label='labels_nucl') # we consider labels on raw segmentation as nuclei, not smoothened out versions
        
        # resize rois
        img.crop_size = self.options['crop_size']
        times['roiprep'] = time.time() - t0
        lbl_nucl_smooth = resizer.run(img, 'nucl')
        times['resizerois'] = time.time() - t0
        
        # calculate nuclei features
        nucl_fts = objext.run(img, track='nucl')
        times['nuclfts'] = time.time() - t0

        # separate nuclei and micronuclei segmentations
        if ('area' in nucl_fts.columns) and (nucl_fts.shape[0] > 0):
            sel = (nucl_fts['area'] <= self.options['isolated_threshold'])
            isolated_idxs = nucl_fts.loc[sel, 'obj_id']
            segm_mni = (img.labels['labels_nucl'] ^ lbl_nucl_smooth) > 0 # extracted micronuclei
            segm_isolated = np.isin(img.labels['labels_nucl'], isolated_idxs.to_list()) # isolated micronuclei
            segm_mni = np.logical_or(segm_mni, segm_isolated) # combine extracted and isolated micronuclei
            lbl_nucl = ~segm_isolated * img.labels['labels_nucl'] # remove isolated micronuclei from nuclei labels
        else:
            segm_mni = clear_border(segm_raw ^ (lbl_nucl_smooth>0))
            lbl_nucl = img.labels['labels_nucl']

        # filter out small micronuclei areas
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
        segm_mni = cv2.erode(segm_mni.astype(np.uint8), kernel, iterations=1)
        segm_mni = cv2.dilate(segm_mni, kernel, iterations=1) > 0

        # update imageset
        img.layers = { 
            'channels': {
                **img.channels
                },
            'segmentations': {
                **img.segmentations, 
                'segmentation_mni': segm_mni
                },
            'labels': {
                'labels_nucl': lbl_nucl
                }
            }
        img.labels_from_segmentation(segmentation='segmentation_mni', labels_name='labels_mni', exclude_border=True)
        img.add_track('mni', channel='mip', segmentation='segmentation_mni', label='labels_mni')
        times['mnifilter'] = time.time() - t0

        # calculate micronuclei features
        mni_fts = objext.run(img, track='mni')
        times['mnifts'] = time.time() - t0

        self.add_output('labels_nucl', img.labels['labels_nucl'])
        self.add_output('labels_mni', img.labels['labels_mni'])

        # assign micronuclei-nuclei pairs
        times['labelsprep'] = time.time() - t0
        nn = findnn.run(img)
        self.add_output('nn', nn)
        times['findnn'] = time.time() - t0
        
        # merge features
        if (mni_fts.shape[0] == 0) or (nucl_fts.shape[0] == 0) or (not nn.columns.str.contains('seed_id').any()):
            merge = pd.DataFrame()
        else:
            merge = pd.merge(mni_fts, nn[['seed_id', 'neighbor_id', 'distance']], left_on='obj_id', right_on='seed_id', how='left')
            merge = pd.merge(merge, nucl_fts, left_on='neighbor_id', right_on='obj_id', how='left', suffixes=['_mni', '_nucl'])

            times['merges'] = time.time() - t0
        tf = time.time()

        self.add_output('nucl_fts', nucl_fts)
        self.add_output('mni_fts', merge)
        self.add_output('img', img)
        print('TOT TIME {}'.format(tf-t0))

        for k,v in times.items():
            print(k,v)

        # logging
        logging.debug('execution time: {:.2f}'.format(tf-t0))
        
        return True


class MicronucleiClassification(Pipeline):

    def __init__(self, config):
        super().__init__(config)
        default_options = { 
            'pixel_segmentation_threshold': .5,
            'crop_size': 300,
            'isolated_threshold': 1500,
            'micronuclei_classification_threshold': 0.5,
            }
        for k,v in default_options.items():
            if k not in self.options.keys():
                self.options[k] = v

    def run(self, img, filename=None):

        if len(self.output) > 0:
            self.output = {}

        filename = 'img' if filename is None else filename
        
        # get workers and input
        pixext, objext, resizer, findnn, wat, pix_clf, mni_clf = self.workers.get_workers()
        img = image.ImageSet(name=filename, layers={'c': {'mip': img}})

        times = {}
        t0 = time.time()

        # semantic pixel classification
        # feature extraction
        pix_fts = pixext.run(img, 'mip', dataframe=True)
        times['pixfts'] = time.time() - t0
        # prediction
        pred = pix_clf.predict(pix_fts)[:,1].reshape(img.shape)
        times['pixpred'] = time.time() - t0
        segm_raw = pred >= self.options['pixel_segmentation_threshold']

        # refine segmentation (close operation)
        segm_raw = cv2.dilate(segm_raw.astype(np.uint8), kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)), iterations=1)
        segm_raw = cv2.erode(segm_raw.astype(np.uint8), kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)), iterations=1)

        # add segmentation
        img.segmentations = {'segmentation_raw': segm_raw>0}
        img.labels_from_segmentation(segmentation='segmentation_raw', labels_name='labels_raw', exclude_border=True)
        img.add_track('raw', channel='mip', segmentation='segmentation_raw', label='labels_raw')
        self.add_output('labels_raw', img.labels['labels_raw'])

        # watershed
        wat_lbls = wat.run(img, track='raw')
        img.labels = {**img.labels, 'labels_nucl': wat_lbls}
        self.add_output('watershed', img.labels['labels_nucl'])
        img.add_track('nucl', channel='mip', label='labels_nucl') # we consider labels on raw segmentation as nuclei, not smoothened out versions
        
        # resize rois
        img.crop_size = self.options['crop_size']
        times['roiprep'] = time.time() - t0
        lbl_nucl_smooth = resizer.run(img, 'nucl')
        times['resizerois'] = time.time() - t0
        
        # calculate nuclei features
        nucl_fts = objext.run(img, track='nucl')
        times['nuclfts'] = time.time() - t0

        # separate nuclei and micronuclei segmentations
        if ('area' in nucl_fts.columns) and (nucl_fts.shape[0] > 0):
            sel = (nucl_fts['area'] <= self.options['isolated_threshold'])
            isolated_idxs = nucl_fts.loc[sel, 'obj_id']
            segm_mni = (img.labels['labels_nucl'] ^ lbl_nucl_smooth) > 0 # extracted micronuclei
            segm_isolated = np.isin(img.labels['labels_nucl'], isolated_idxs.to_list()) # isolated micronuclei
            segm_mni = np.logical_or(segm_mni, segm_isolated) # combine extracted and isolated micronuclei
            lbl_nucl = ~segm_isolated * img.labels['labels_nucl'] # remove isolated micronuclei from nuclei labels
        else:
            segm_mni = clear_border(segm_raw ^ (lbl_nucl_smooth>0))
            lbl_nucl = img.labels['labels_nucl']

        # filter out small micronuclei areas
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
        segm_mni = cv2.erode(segm_mni.astype(np.uint8), kernel, iterations=1)
        segm_mni = cv2.dilate(segm_mni, kernel, iterations=1) > 0

        # update imageset
        img.layers = { 
            'channels': {
                **img.channels
                },
            'segmentations': {
                **img.segmentations, 
                'segmentation_mni': segm_mni
                },
            'labels': {
                'labels_nucl': lbl_nucl
                }
            }
        img.labels_from_segmentation(segmentation='segmentation_mni', labels_name='labels_mni', exclude_border=True)
        img.add_track('mni', channel='mip', segmentation='segmentation_mni', label='labels_mni')
        times['mnifilter'] = time.time() - t0

        # calculate micronuclei features
        mni_fts = objext.run(img, track='mni')
        times['mnifts'] = time.time() - t0

        self.add_output('labels_nucl', img.labels['labels_nucl'])
        self.add_output('labels_mni', img.labels['labels_mni'])

        # assign micronuclei-nuclei pairs
        times['labelsprep'] = time.time() - t0
        nn = findnn.run(img)
        self.add_output('nn', nn)
        times['findnn'] = time.time() - t0
        
        # merge features
        if (mni_fts.shape[0] == 0) or (nucl_fts.shape[0] == 0) or (not nn.columns.str.contains('seed_id').any()):
            merge = pd.DataFrame()
        else:
            merge = pd.merge(mni_fts, nn[['seed_id', 'neighbor_id', 'distance']], left_on='obj_id', right_on='seed_id', how='left')
            merge = pd.merge(merge, nucl_fts, left_on='neighbor_id', right_on='obj_id', how='left', suffixes=['_mni', '_nucl'])
            times['merges'] = time.time() - t0
        tf = time.time()

        self.add_output('nucl_fts', nucl_fts)
        self.add_output('mni_fts', merge)
        self.add_output('img', img)
        print('TOT TIME {}'.format(tf-t0))

        for k,v in times.items():
            print(k,v)

        # logging
        logging.debug('execution time: {:.2f}'.format(tf-t0))

        # remove mni without parent
        if merge.shape[0] > 0:
            merge = merge.loc[~merge['obj_id_nucl'].isna(), :]
        
        # classify
        if merge.shape[0] == 0:
            logging.info('empty image')
            return False
        
        pred = mni_clf.predict(merge)[:,1]

        merge['pred_proba'] = pred
        prob_table = merge[['obj_id_nucl', 'pred_proba']].sort_values(by='pred_proba')
        prob_table = prob_table[prob_table['pred_proba'] >= self.options['micronuclei_classification_threshold']]
        nucl_ids = prob_table.reset_index().loc[:9, 'obj_id_nucl']
        out = (np.isin(img.labels['labels_nucl'], nucl_ids) * 255).astype('uint8')
        print('pred size =', out.shape)
        print('MNI found: {}'.format(nucl_ids))
        
        for i in prob_table['pred_proba'].sort_values().iloc[:9].values:
            logging.info('object detected {:.4f}'.format(i))

        self.add_output('pred_mni', out) 

        # logging mni images
        img.crop_size = 400
        if len(nucl_ids) == 0:
            return True
        sel_nucl = img.get_roi(nucl_ids, track='nucl', square=True, neighborhood=50)
        pkgdir = os.path.dirname(__path__[0]) 
        savedir = os.path.join(pkgdir, 'logs', 'images')

        if len(os.listdir(savedir)) > 0:
            n = np.max([int(os.path.splitext(os.path.basename(i))[0]) for i in os.listdir(savedir)]) + 1
        else:
            n = 1

        if not isinstance(sel_nucl, list):
            sel_nucl = [sel_nucl]
        for i in sel_nucl:
            pkgdir = os.path.dirname(__path__[0]) 
            savepath = os.path.join(savedir, '{}.png'.format(str(n).zfill(4)))
            n += 1
            imsave(arr=i.channel, fname=savepath)

        return True


class RoundClassification_CombinedFeatures(Pipeline):
    """
    Class adapted from MicronucleiClassification(), taking into consideration micronuclei features to avoid classifying micronucleated cells as round nuclei.
    """
    def __init__(self, config):
        super().__init__(config)
        default_options = { 
            'pixel_segmentation_threshold': .5,
            'crop_size': 300,
            'round_classification_threshold': 0.85,
            }
        for k,v in default_options.items():
            if k not in self.options.keys():
                self.options[k] = v

    def run(self, img, filename=None):

        if len(self.output) > 0:
            self.output = {}

        filename = 'img' if filename is None else filename

        # get workers and input
        pixext, objext, resizer, findnn, wat, pix_clf, mni_clf = self.workers.get_workers()
        img = image.ImageSet(name=filename, layers={'c': {'mip': img}})

        times = {}
        t0 = time.time()

        # semantic pixel classification
        # feature extraction
        pix_fts = pixext.run(img, 'mip', dataframe=True)
        times['pixfts'] = time.time() - t0
        # prediction of pixel classifier
        pred = pix_clf.predict(pix_fts)[:,1].reshape(img.shape)
        times['pixpred'] = time.time() - t0
        segm_raw = pred >= self.options['pixel_segmentation_threshold']

        # refine segmentation (close operation)
        segm_raw = cv2.dilate(segm_raw.astype(np.uint8), kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)), iterations=1)
        segm_raw = cv2.erode(segm_raw.astype(np.uint8), kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)), iterations=1)

        # add segmentation
        img.segmentations = {'segmentation_raw': segm_raw>0}
        img.labels_from_segmentation(segmentation='segmentation_raw', labels_name='labels_raw', exclude_border=True)
        img.add_track('raw', channel='mip', segmentation='segmentation_raw', label='labels_raw')
        self.add_output('labels_raw', img.labels['labels_raw'])

        # watershed
        wat_lbls = wat.run(img, track='raw')
        img.labels = {**img.labels, 'labels_nucl': wat_lbls}
        self.add_output('watershed', img.labels['labels_nucl'])
        img.add_track('nucl', channel='mip', label='labels_nucl') # we consider labels on raw segmentation as nuclei, not smoothened out versions
        
        # resize rois
        img.crop_size = self.options['crop_size']
        times['roiprep'] = time.time() - t0
        lbl_nucl_smooth = resizer.run(img, 'nucl')
        times['resizerois'] = time.time() - t0
        
        # calculate nuclei features
        nucl_fts = objext.run(img, track='nucl')
        times['nuclfts'] = time.time() - t0

        # separate nuclei and micronuclei segmentations
        if ('area' in nucl_fts.columns) and (nucl_fts.shape[0] > 0):
            sel = (nucl_fts['area'] <= self.options['isolated_threshold'])
            isolated_idxs = nucl_fts.loc[sel, 'obj_id']
            segm_mni = (img.labels['labels_nucl'] ^ lbl_nucl_smooth) > 0 # extracted micronuclei
            segm_isolated = np.isin(img.labels['labels_nucl'], isolated_idxs.to_list()) # isolated micronuclei
            segm_mni = np.logical_or(segm_mni, segm_isolated) # combine extracted and isolated micronuclei
            lbl_nucl = ~segm_isolated * img.labels['labels_nucl'] # remove isolated micronuclei from nuclei labels
        else:
            segm_mni = clear_border(segm_raw ^ (lbl_nucl_smooth>0))
            lbl_nucl = img.labels['labels_nucl']

        # filter out small micronuclei areas
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
        segm_mni = cv2.erode(segm_mni.astype(np.uint8), kernel, iterations=1)
        segm_mni = cv2.dilate(segm_mni, kernel, iterations=1) > 0

        # update imageset
        img.layers = { 
            'channels': {
                **img.channels
                },
            'segmentations': {
                **img.segmentations, 
                'segmentation_mni': segm_mni
                },
            'labels': {
                'labels_nucl': lbl_nucl
                }
            }
        img.labels_from_segmentation(segmentation='segmentation_mni', labels_name='labels_mni', exclude_border=True)
        img.add_track('mni', channel='mip', segmentation='segmentation_mni', label='labels_mni')
        times['mnifilter'] = time.time() - t0

        # calculate micronuclei features
        mni_fts = objext.run(img, track='mni')
        times['mnifts'] = time.time() - t0

        self.add_output('labels_nucl', img.labels['labels_nucl'])
        self.add_output('labels_mni', img.labels['labels_mni'])

        # assign micronuclei-nuclei pairs
        times['labelsprep'] = time.time() - t0
        nn = findnn.run(img)
        self.add_output('nn', nn)
        times['findnn'] = time.time() - t0

        # merge features
        if (mni_fts.shape[0] == 0) or (nucl_fts.shape[0] == 0) or (not nn.columns.str.contains('seed_id').any()):
            merge = pd.DataFrame()
        else:
            # merge results of nearest neighbor to the micronuclear features
            merge = pd.merge(mni_fts, nn[['seed_id', 'neighbor_id', 'distance']], left_on='obj_id', right_on='seed_id', how='left')
            # remove nuclear features from mni feature table #TODO: possibly redundant (see merge later on)
            merge = merge[merge.columns.drop(list(merge.filter(regex='_nucl')))]
            # rename columns in mni feature table to remove suffixes
            merge.columns = merge.columns.str.replace('_mni', '')
            # merge nuclear and micronuclear features
            merge = pd.merge(nucl_fts, merge, left_on=['obj_id', 'filename'], right_on=['neighbor_id', 'filename'], how='left', suffixes=['_nucl', '_mni'])

            times['merges'] = time.time() - t0
        tf = time.time()

        self.add_output('nucl-mni_fts', merge)
        self.add_output('img', img)
        print('TOT TIME {}'.format(tf-t0))

        for k,v in times.items():
            print(k,v)

        # logging
        logging.debug('execution time: {:.2f}'.format(tf-t0))

        pred = mni_clf.predict(merge)[:,1]

        merge['pred_proba'] = pred
        prob_table = merge[['obj_id_nucl', 'pred_proba']].sort_values(by='pred_proba')
        prob_table = prob_table[prob_table['pred_proba'] >= self.options['round_classification_threshold']]
        nucl_ids = prob_table.reset_index().loc[:9, 'obj_id_nucl']
        out = (np.isin(img.labels['labels_nucl'], nucl_ids) * 255).astype('uint8')
        print('pred size =', out.shape)
        print('ROUND NUCLEI found: {}'.format(nucl_ids))
        for i in prob_table['pred_proba'].sort_values().iloc[:9].values:
            logging.info('object detected {:.4f}'.format(i))

        self.add_output('pred_rnd', out) 
        return True


class RoundClassification(Pipeline):
    def __init__(self, config):
        super().__init__(config)
        default_options = { 
            'pixel_segmentation_threshold': .5,
            'crop_size': 300,
            'round_classification_threshold': 0.9,
            }
        for k,v in default_options.items():
            if k not in self.options.keys():
                self.options[k] = v

    def run(self, img, filename=None):

        if len(self.output) > 0:
            self.output = {}

        filename = 'img' if filename is None else filename
        
        # get workers and input
        pixext, objext, wat, pix_clf, rnd_clf = self.workers.get_workers()
        img = image.ImageSet(name=filename, layers={'c': {'mip': img}})

        times = {}
        t0 = time.time()

        # semantic pixel classification
        # feature extraction
        pix_fts = pixext.run(img, 'mip', dataframe=True)
        times['pixfts'] = time.time() - t0
        # prediction
        pred = pix_clf.predict(pix_fts)[:,1].reshape(img.shape)
        times['pixpred'] = time.time() - t0
        segm_raw = pred >= self.options['pixel_segmentation_threshold']

        # refine segmentation (close operation)
        segm_raw = cv2.dilate(segm_raw.astype(np.uint8), kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)), iterations=1)
        segm_raw = cv2.erode(segm_raw.astype(np.uint8), kernel=cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4)), iterations=1)

        # add segmentation
        img.segmentations = {'segmentation_raw': segm_raw>0}
        img.labels_from_segmentation(segmentation='segmentation_raw', labels_name='labels_raw', exclude_border=True)
        img.add_track('raw', channel='mip', segmentation='segmentation_raw', label='labels_raw')
        self.add_output('labels_raw', img.labels['labels_raw'])

        # watershed
        wat_lbls = wat.run(img, track='raw')
        img.labels = {**img.labels, 'labels_nucl': wat_lbls}
        self.add_output('watershed', img.labels['labels_nucl'])
        img.add_track('nucl', channel='mip', label='labels_nucl') # we consider labels on raw segmentation as nuclei, not smoothened out versions
        
        # calculate nuclei features
        nucl_fts = objext.run(img, track='nucl')
        times['nuclfts'] = time.time() - t0

        pred = rnd_clf.predict(nucl_fts)[:,1]

        nucl_fts['pred_proba'] = pred
        prob_table = nucl_fts[['obj_id', 'pred_proba']].sort_values(by='pred_proba')
        prob_table = prob_table[prob_table['pred_proba'] >= self.options['round_classification_threshold']]
        nucl_ids = prob_table.reset_index().loc[:9, 'obj_id']
        out = (np.isin(img.labels['labels_nucl'], nucl_ids) * 255).astype('uint8')
        print('pred size =', out.shape)
        print('ROUND NUCLEI found: {}'.format(nucl_ids))
        for i in prob_table['pred_proba'].sort_values().iloc[:9].values:
            logging.info('object detected {:.4f}'.format(i))

        self.add_output('pred_rnd', out) 
        return True